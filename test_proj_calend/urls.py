from django.conf.urls import url
from django.urls import path

from . import views


urlpatterns = [
    path('', views.home, name='home'),
    path('user_page/', views.user_page, name='user_page'),
    path('view_events/', views.view_events, name='view_events'),
    path('view_holidays', views.view_holidays, name='view_holidays'),
    path('signup/', views.registration, name='registration'),
    path('login/', views.login_func, name='login'),
    path('logout/', views.logout_func, name='logout'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate_acc, name='activate_acc'),
]
