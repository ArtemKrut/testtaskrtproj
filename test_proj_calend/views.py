import datetime
import re
import requests

from ics import Calendar

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.mail import EmailMessage, send_mail
from django.contrib import auth
from django.conf import settings
from django.db.models import Q

from apscheduler.schedulers.background import BackgroundScheduler

from dateutil.relativedelta import relativedelta

from .models import PlannedThing, LoginUsers, UsersCountry
from .forms import RegistrationForm, LoginForm, RequiredDateForm, SortingDataForm, UserCountryForm
from .tokens import account_activation_token


def home(request):
    """
    Отображение главной страницы в зависимости от того, авторизован ли пользователь.
    """
    if request.user.is_authenticated:
        return redirect('user_page')
    else:
        return render(request, 'home.html')


def registration(request):
    """
    Регистрация нового пользователя.
    При регистрации проверяется существование пользователя в базе данных и при наличии, регистрация не проходит.
    При отсутствии введенного пользователя в базе данных, отправляется писмо на введенную почту, для подтверждения.
    """
    if request.method == 'POST':
        registration_form = RegistrationForm(request.POST)
        user_country = UserCountryForm(request.POST)
        if registration_form.is_valid() and user_country.is_valid():
            user = registration_form.save(commit=False)
            user.is_active = False
            user.save()

            selected_user_country = user_country.cleaned_data['country']

            country_model = UsersCountry(None, user, selected_user_country)
            country_model.save()

            current_site = get_current_site(request)
            message = render_to_string('acc_active_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })

            mail_subject = 'Activate your account.'
            to_email = registration_form.cleaned_data.get('email')
            email = EmailMessage(mail_subject, message, to=[to_email])
            email.send()

            return render(request, 'acc_active_sent.html')

    else:
        registration_form = RegistrationForm()
        user_country = UserCountryForm()
    context = {
        'form': registration_form,
        'country_form': user_country
    }
    return render(request, 'registration.html', context)


def activate_acc(request, uidb64, token):
    """
    Активация аккаунта по почте через ссылку, отправленную на введеннуб почту
    """
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)

    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()

        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')


def login_func(request):
    """
    Вход пользователья на сайт.
    Пользователь может ввести в поле Username почту или логин, после чего при введении пароля, авторизоваться.
    """
    form = LoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")

        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)

            model = LoginUsers(None, request.user.username, request.user.email)
            model.save()

            return redirect('user_page')

        else:
            context = {'form': form,
                       'error': 'The username and password combination is incorrect'}

            return render(request, 'login.html', context)

    else:
        context = {'form': form}
        return render(request, 'login.html', context)


def logout_func(request):
    """
    Выход пользователя с сайта
    """
    if request.user.is_authenticated:
        auth.logout(request)
        LoginUsers.objects.all().delete()
        return redirect('home')
    else:
        return redirect('home')


def user_page(request):
    """
    Отображение страницы с формой для создания события.
    При создании события, нельзя создать (сохранить в б/д) событие, у которого дата окончания меньше даты начала.
    """
    if request.user.is_authenticated:
        if request.method == 'POST':
            form = RequiredDateForm(request.POST)

            if form.is_valid():
                user = request.user
                name_of_thing = form.cleaned_data['name_of_thing']
                if not form.cleaned_data['end_date']:
                    end_day = datetime.datetime.now().strftime('%Y-%m-%d ') + '23:59'
                else:
                    end_day = form.cleaned_data['end_date'].strftime('%Y-%m-%d %H:%M')
                start_day = form.cleaned_data['start_date'].strftime('%Y-%m-%d %H:%M')
                print(end_day)
                warning_date = form.cleaned_data['warning_date']

                warning_date_number = int(re.search(r'\d+', warning_date).group(0))

                date_in_datetime_format = datetime.datetime.strptime(start_day, '%Y-%m-%d %H:%M')

                if warning_date.startswith('days'):
                    not_changed_warning_days = date_in_datetime_format - relativedelta(days=warning_date_number)

                    warning_in_days = not_changed_warning_days.strftime('%Y-%m-%d %H:%M')

                    save_req_model(user, name_of_thing, start_day, end_day, warning_in_days)

                elif warning_date.startswith('hour'):
                    not_changed_warning_days = date_in_datetime_format - relativedelta(hours=warning_date_number)

                    warning_in_hours = not_changed_warning_days.strftime('%Y-%m-%d %H:%M')

                    save_req_model(user, name_of_thing, start_day, end_day, warning_in_hours)

        data = PlannedThing.objects.filter(user_name=request.user)

        required_date_form = RequiredDateForm()
        context = {'req_form': required_date_form,
                   'data': data}

        return render(request, 'user_page.html', context)
    else:
        return redirect('home')


def save_req_model(user, name_of_thing, start_date_of_thing, end_date_of_thing, warning_for_date):
    """
    Сохранение введенного события в базу данных
    """
    model = PlannedThing(None, user, name_of_thing, start_date_of_thing, end_date_of_thing, warning_for_date)
    model.save()


def view_events(request):
    """
    Сортировка введенных пользователем событий.
    Имеется возможность просмотреть все события, отсортированные по дням
    """
    if request.user.is_authenticated:
        if request.method == 'POST':
            date_form = SortingDataForm(request.POST)
            if date_form.is_valid():
                date_years = int(date_form.cleaned_data['year_date_field'])
                date_months = int(date_form.cleaned_data['month_date_field'])
                date_days = int(date_form.cleaned_data['day_date_field'])

                month = check_month(date_months)
                day = check_day(date_days)
                year = str(date_years)

                if date_days != 0:
                    data = PlannedThing.objects.filter(
                        Q(user_name=request.user),
                        Q(start_date_of_thing__startswith=f'{year}-{month}-{day}')
                    )

                    context = {'date_form': date_form, 'data': data}
                    return render(request, 'view_events.html', context)
                else:
                    data = PlannedThing.objects.filter(
                        Q(user_name=request.user),
                        Q(start_date_of_thing__startswith=f'{year}-{month}')
                    )

                    context = {'date_form': date_form, 'data': data}
                    return render(request, 'view_events.html', context)

        date_form = SortingDataForm()

        context = {'date_form': date_form}
        return render(request, 'view_events.html', context)
    else:
        return redirect('home')


def check_month(month):
    """
    Проверка ввода месяца в выпадающем меню для верного отображения сортировки
    """
    if month < 10:
        return '0' + str(month)
    else:
        return str(month)


def check_day(day):
    """
    Проверка ввода дня в выпадающем меню для верного отображения сортировки
    """
    if day < 10:
        return '0' + str(day)
    else:
        return str(day)


def view_holidays(request):
    """
    Отображение всех праздников страны вошедшего пользователя
    """
    if request.user.is_authenticated:
        country = UsersCountry.objects.filter(Q(user=request.user))[0].country

        url = f"https://www.officeholidays.com/ics/{country}"
        calendar_of_holidays = Calendar(requests.get(url).text)

        all_user_holidays = []
        for holiday in calendar_of_holidays.timeline:
            all_user_holidays.append(
                {'name': holiday.name.split(':')[1],
                 'start_date': holiday.begin.format('YYYY-MM-DD HH:MM'),
                 'end_date': holiday.end.format('YYYY-MM-DD HH:MM')
                 })

        context = {'users_country': country.title(),
                   'country_holidays': all_user_holidays}
        return render(request, 'view_country_hol.html', context)
    else:
        return redirect('home')


def info_for_email():
    """
    Отправка сообщения на введенный адрес email, при наступленни времени события.
    В зависимости от выбранного времени напоминания.
    """
    user_name = LoginUsers.objects.all()[0].user_name
    user_email = LoginUsers.objects.all()[0].email

    data = PlannedThing.objects.filter(user_name=user_name)
    for date in data:
        start_date = date.start_date_of_thing.strftime('%Y-%m-%d %H:%M')
        user_warning_date = date.warning_for_date.strftime('%Y-%m-%d %H:%M')
        server_date_now = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')

        if user_warning_date == server_date_now:
            text_of_message = f"""\n
Hello {date.user_name}, \nyour event '{date.name_of_thing}' will happen soon.\n
Start at {start_date}.
Thanks.
                    """

            send_mail(f'{date.name_of_thing} event', text_of_message,
                      settings.EMAIL_HOST_USER, [user_email])


scheduler = BackgroundScheduler()
scheduler.add_job(info_for_email, 'interval', seconds=60)
scheduler.start()
