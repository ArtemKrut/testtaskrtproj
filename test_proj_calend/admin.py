from django.contrib import admin

from .models import PlannedThing, LoginUsers, UsersCountry


@admin.register(PlannedThing)
class RequiredUser(admin.ModelAdmin):
    exclude = ('author',)

    def save_model(self, request, obj, form, change):
        if not obj.pk:
            obj.author = request.user
        super().save_model(request, obj, form, change)


admin.site.register(LoginUsers)
admin.site.register(UsersCountry)