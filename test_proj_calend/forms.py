from datetime import timezone, datetime

import pytz

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import DateInput
from django.utils import timezone

from .formats import WARNING_DATES, YEARS, MONTHS, DAYS, COUNTRIES, DATES_FORMATS


class RegistrationForm(UserCreationForm):
    """
    Форма регистрации нового пользователя
    """
    email = forms.EmailField(max_length=200, help_text='Required')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        if email and User.objects.filter(email=email).exists():
            raise ValidationError('Email already exists')
        return email


class UserCountryForm(forms.Form):
    country = forms.ChoiceField(choices=COUNTRIES, label='User country:')


class LoginForm(forms.Form):
    """
    Форма для входа пользователя на сайт
    """
    username = forms.CharField(max_length=25, label='Enter username')
    password = forms.CharField(max_length=30, label='Enter password', widget=forms.PasswordInput)


class RequiredDateForm(forms.Form):
    """
    Форма для создания события
    """
    name_of_thing = forms.CharField(max_length=30, label='Enter name of event:')
    start_date = forms.DateTimeField(input_formats=DATES_FORMATS,
                                     widget=DateInput(attrs={'type': 'datetime-local'}),
                                     label='Start date of the event:')
    end_date = forms.DateTimeField(input_formats=DATES_FORMATS, required=False,
                                   widget=DateInput(attrs={'type': 'datetime-local'}),
                                   label='End date of the event:')
    warning_date = forms.ChoiceField(choices=WARNING_DATES, label='Warning time:')

    def clean(self):
        cleaned_data = super(RequiredDateForm, self).clean()

        end_date = cleaned_data['end_date']
        start_date = cleaned_data['start_date']

        if end_date is None:
            now = timezone.now().strftime('%d-%m-%Y ') + '23:59'
            end_date = datetime.strptime(now, '%d-%m-%Y %H:%M')
            aware_start_date = pytz.utc.localize(end_date)

            if start_date > aware_start_date:
                raise ValidationError('End date should be greater than start date.')
        else:
            if start_date > start_date:
                raise ValidationError('End date should be greater than start date.')
        return cleaned_data


class SortingDataForm(forms.Form):
    """
    Форма для сортирвоки существующих событий
    """
    year_date_field = forms.ChoiceField(choices=YEARS)
    month_date_field = forms.ChoiceField(choices=MONTHS)
    day_date_field = forms.ChoiceField(choices=DAYS)
