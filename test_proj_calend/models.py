from django.db import models


class PlannedThing(models.Model):
    """
    Модель для создания события
    """
    user_name = models.CharField(max_length=30, default="AnonymousUser", editable=False)
    name_of_thing = models.CharField(max_length=30)
    start_date_of_thing = models.DateTimeField(null=True, blank=True)
    end_date_of_thing = models.DateTimeField(null=True, blank=True)
    warning_for_date = models.DateTimeField(null=True, blank=True)


class LoginUsers(models.Model):
    """
    Модель для вошедшего пользователя
    """
    user_name = models.CharField(max_length=30)
    email = models.CharField(max_length=50, default="")


class UsersCountry(models.Model):
    """
    Модель для сопоставления пользователя и его страны
    """
    user = models.CharField(max_length=50, default="AnonymousUser")
    country = models.CharField(max_length=50, blank=True)